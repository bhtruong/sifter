# Sifter & Friends

<a href="https://www.curseforge.com/minecraft/mc-mods/fabric-api">
  <img src="https://i.imgur.com/Ol1Tcf8.png"
       width="95"
       alt="Requires Fabric API"/>
</a>

Do you hate repetitively breaking gravel for flint?
With a sifter, hate no more!  
Also adds fertilizer and other goodies.

# Additions

### Sifter

<img src="media/recipes/sifter.png"
     width="256"
     alt="Craft from 5x strings and 4x sticks.">

### Saltpeter

Sifted from sand, and in greater quantities from clay.

### Pyrite

Mined from coal, and in greater quantities from quartz.  
Being *fools gold*, it can also be crafted into a block that is
visually similar to gold.

<img src="media/recipes/pyrite_block.png"
     width="256"
     alt="Craft from 9x pyrite">

Smelt pyrite in a blast furnace to get a iron nuggets,
or smelt blocks of pyrite to get iron ingots.

<img src="media/recipes/iron_nugget.png" width="180">
<img src="media/recipes/iron_ingot.png" width="180">

### Sulfur

Smelt pyrite in a normal furnace for sulfur.

<img src="media/recipes/sulfur.png" width="180">

### Gunpowder

Combine sulfur, saltpeter, and char/coal for gunpowder,
just like in [ye olden days](https://en.wikipedia.org/wiki/Gunpowder).

<img src="media/recipes/gunpowder.png"
     width="256"
     alt="Craft from 7x saltpeter, sulfur, and char/coal">

### Fertilizer

For the horticulturist, fertilizer is a stronger bone meal alternative
that has a 3 x 3 area of effect.

<img src="media/recipes/fertilizer.png"
     width="160"
     alt="Craft from sulfur, 2x saltpeter, and bonemeal">

# Licensing

Copyright (C) 2020 Brian Truong (CosmicConifer)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.