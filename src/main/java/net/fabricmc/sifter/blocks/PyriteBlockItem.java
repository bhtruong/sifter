/**********

Pyrite block item

**********/

package net.fabricmc.sifter.blocks;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;

public class PyriteBlockItem extends BlockItem
{
    public PyriteBlockItem(Block block)
    {
        super
        (
            block,
            new Settings().group(ItemGroup.BUILDING_BLOCKS)
        );
    }
}