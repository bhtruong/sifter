/**********

Pyrite block

**********/

package net.fabricmc.sifter.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.Material;

import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tools.FabricToolTags;

public class PyriteBlock extends Block
{
    public PyriteBlock()
    {
        super
        (
            FabricBlockSettings.of(Material.METAL)
                               .hardness(3)
                               .drops(ModBlocks.PYRITE_BLOCK_ID)
                               .breakByTool(FabricToolTags.PICKAXES, 2)
                               .build()
        );
    }
}