/**********

Registers all of the blocks in the mod

**********/

package net.fabricmc.sifter.blocks;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModBlocks
{
    public static final Identifier PYRITE_BLOCK_ID = new Identifier("sifter","pyrite_block");
    public static final Block PYRITE_BLOCK = new PyriteBlock();
    public static final BlockItem PYRITE_BLOCK_ITEM = new PyriteBlockItem(PYRITE_BLOCK);

    public static void register()
    {
        Registry.register(Registry.BLOCK, PYRITE_BLOCK_ID, PYRITE_BLOCK);
        Registry.register(Registry.ITEM, PYRITE_BLOCK_ID, PYRITE_BLOCK_ITEM);
    }
}