/**********

Fertilizer item

A powerful version of bonemeal that affects a 3x3 area

**********/

package net.fabricmc.sifter.items;

import net.minecraft.block.Blocks;
import net.minecraft.block.Block;
import net.minecraft.block.Fertilizable;
import net.minecraft.item.BoneMealItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.ActionResult;

public class Fertilizer extends BoneMealItem
{
    public Fertilizer(Settings settings)
    {
        super(settings.group(ItemGroup.MISC)
                      .maxCount(64));
    }

    private Block blockAt(World world, BlockPos pos)
    {
        return world.getBlockState(pos).getBlock();
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext ctx)
    {
        World world = ctx.getWorld();
        PlayerEntity player = ctx.getPlayer();
        BlockPos pos = ctx.getBlockPos();
        ItemStack stack = ctx.getStack();

        if (player == null || !(blockAt(world, pos) instanceof Fertilizable))
        {
            return ActionResult.PASS;
        }

        // Default bone meal behavior when applied to grass block
        else if (blockAt(world, pos).equals(Blocks.GRASS_BLOCK))
        {
            BoneMealItem.useOnFertilizable(stack, world, pos);
            return ActionResult.PASS;
        }

        // Fertilize area
        int dim = 3;
        int origin = 0 - (dim / 2);
        for (int x = 0; x < dim; ++x)
        for (int y = 0; y < dim; ++y)
        for (int z = 0; z < dim; ++z)
        {
            int dx = origin + x;
            int dz = origin + z;
            int dy = origin + y;
            BlockPos p = pos.add(dx, dy, dz);

            if (blockAt(world, p).equals(Blocks.GRASS_BLOCK)
                || blockAt(world, p).equals(Blocks.GRASS))
            {
                continue;
            }

            int particles = 3;
            int repeat = dim - Math.abs(dx) - Math.abs(dz);

            for (int j = 0; j < repeat; ++j)
            {
                if (BoneMealItem.useOnFertilizable(player.getActiveItem(), world, p))
                {
                    BoneMealItem.createParticles(world, p, particles);
                }
                else
                {
                    break;
                }
            }
        }

        if (!player.abilities.creativeMode)
        {
            stack.decrement(1);
        }

        return ActionResult.SUCCESS;
    }
}