/**********

Sifting tool item

Allows for the extraction of fine materials

**********/

package net.fabricmc.sifter.items;

import net.minecraft.item.ToolMaterials;
import net.minecraft.item.ToolMaterial;
import net.minecraft.item.ToolItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Block;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.sound.SoundEvents;
import net.minecraft.sound.SoundCategory;

public class Sifter extends ToolItem
{
    private static final int MAX_DAMAGE = 48;

    public Sifter(Settings settings)
    {
        super
        (
            ToolMaterials.WOOD,
            settings.group(ItemGroup.TOOLS)
                    .maxCount(1)
                    .maxDamage(MAX_DAMAGE)
        );
    }

    // Deal with durability
    public boolean postMine
    (
        ItemStack stack,
        World world,
        BlockState state,
        BlockPos pos,
        LivingEntity miner
    )
    {
        // Apply durability damage
        int damage = this.isEffectiveOn(state) ? 1 : 2;
        stack.damage(damage, RANDOM, null);

        // Break tool
        if (stack.getDamage() >= MAX_DAMAGE)
        {
            world.playSound(
                null, pos,
                SoundEvents.ENTITY_ITEM_BREAK,
                SoundCategory.PLAYERS,
                1.0F, 1.0F);
            stack.decrement(1);
        }

        // Not sure what this indicates
        return true;
    }

    // NOTE: true => ore block will drop
    public boolean isEffectiveOn(BlockState state)
    {
        Block block = state.getBlock();
        return block.equals(Blocks.GRAVEL)
            || block.equals(Blocks.SAND)
            || block.equals(Blocks.CLAY);
    }

    public ToolMaterial getMaterial()
    {
        return ToolMaterials.WOOD;
    }

    public boolean isEnchantable(ItemStack stack)
    {
        return true;
    }

    public int getEnchantability() {
        return 15;
    }

    public float getMiningSpeed(ItemStack stack, BlockState state)
    {
        return 1;
    }
}