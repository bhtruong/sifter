/**********

Generic crafting ingredient item

**********/

package net.fabricmc.sifter.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

public class Ingredient extends Item
{
    public Ingredient(Settings settings)
    {
        super(settings.group(ItemGroup.MISC));
    }
}