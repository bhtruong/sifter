/**********

Registers all of the items in the mod

**********/

package net.fabricmc.sifter.items;

import net.minecraft.item.Item;
import net.minecraft.item.Item.Settings;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModItems
{
    public static final Item SIFTER = new Sifter(new Settings());
    public static final Identifier SIFTER_ID = new Identifier("sifter", "sifter");

    public static final Item PYRITE = new Ingredient(new Settings());
    public static final Identifier PYRITE_ID = new Identifier("sifter", "pyrite");

    public static final Item SALTPETER = new Ingredient(new Settings());
    public static final Identifier SALTPETER_ID = new Identifier("sifter", "saltpeter");

    public static final Item SULFUR = new Ingredient(new Settings());
    public static final Identifier SULFUR_ID = new Identifier("sifter", "sulfur");

    public static final Item FERTILIZER = new Fertilizer(new Settings());
    public static final Identifier FERTILIZER_ID = new Identifier("sifter", "fertilizer");

    public static void register()
    {
        Registry.register(Registry.ITEM, SIFTER_ID, SIFTER);
        Registry.register(Registry.ITEM, PYRITE_ID, PYRITE);
        Registry.register(Registry.ITEM, SALTPETER_ID, SALTPETER);
        Registry.register(Registry.ITEM, SULFUR_ID, SULFUR);
        Registry.register(Registry.ITEM, FERTILIZER_ID, FERTILIZER);
    }
}