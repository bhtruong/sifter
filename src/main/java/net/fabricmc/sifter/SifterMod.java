/**********

Main mod file

**********/

package net.fabricmc.sifter;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.sifter.items.ModItems;
import net.fabricmc.sifter.blocks.ModBlocks;
import net.fabricmc.sifter.loot_tables.OreLoot;

public class SifterMod implements ModInitializer
{
    @Override
    public void onInitialize()
    {
        ModItems.register();
        ModBlocks.register();
        OreLoot.register();
    }
}
