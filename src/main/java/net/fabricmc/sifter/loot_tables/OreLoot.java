/**********

Add new drops to preexisting ores

**********/

package net.fabricmc.sifter.loot_tables;

import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.ConstantLootTableRange;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.function.ApplyBonusLootFunction;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.util.Identifier;

import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;

import net.fabricmc.sifter.items.ModItems;

public class OreLoot
{
    private static final Identifier COAL_ID = new Identifier
    (
        "minecraft", "blocks/coal_ore"
    );

    private static final Identifier QUARTZ_ID = new Identifier
    (
        "minecraft", "blocks/nether_quartz_ore"
    );

    public static void register()
    {
        LootTableLoadingCallback.EVENT.register(
            (resourceManager, lootManager, id, supplier, setter) -> {
                if (COAL_ID.equals(id) || QUARTZ_ID.equals(id))
                {
                    float chance = COAL_ID.equals(id) ? 0.4F : 1.0F;

                    FabricLootPoolBuilder poolBuilder = FabricLootPoolBuilder.builder()
                        .withRolls(ConstantLootTableRange.create(1))
                        .withEntry(ItemEntry.builder(ModItems.PYRITE)
                                            .withCondition(RandomChanceLootCondition.builder(chance)))
                        .withFunction(ApplyBonusLootFunction.oreDrops(Enchantments.FORTUNE));

                    supplier.withPool(poolBuilder);
                }
        });
    }
}